import pandas as pd
from matplotlib import pyplot
from databaseConnection import *
from functions import *


DATA_FILE_PATH = "../Trips_46AO"


def data_cleaning(table_name, route, direction):
    journey_pattern_id = get_journey_pattern_id(route, direction)
    data = get_gps_data_for_route_and_direction(table_name, journey_pattern_id)
    data = assign_osm_geom(data)
    route_data = get_data_without_outliers(data, journey_pattern_id)
    file_name = get_route_data_file_name(journey_pattern_id)
    write_dataframe_to_csv(route_data, file_name)
    generate_table_for_route(journey_pattern_id, file_name)
    max_long, min_long, max_lat, min_lat = get_extreme_lat_long(route_data)
    osm_point_table_name, osm_line_table_name = generate_osm_data(journey_pattern_id, min_lat, max_lat, min_long,
                                                                  max_long)

    # data_with_osm_line_file_name = assign_nearest_osm_line_to_route_data(journey_pattern_id, osm_line_table_name,
    #                                                                      999999999)

    data_with_osm_point_file_name = assign_nearest_osm_point_to_route_data(journey_pattern_id, osm_point_table_name,
                                                                           999999999)

    undiverted_trips_data = get_gps_data_of_undiverted_trips(data_with_osm_line_file_name, journey_pattern_id)



def get_journey_pattern_id(route, direction):
    if direction == 'O':
        direction_code = '0001'
    else:
        direction_code = '1001'

    journey_pattern_id = "0%s%s" % (route, direction_code)

    return journey_pattern_id


def get_gps_data_for_route_and_direction(table_name, journey_pattern_id):
    print("Generating GPS data for route and direction")
    trip_table_name = "trips_%s" % journey_pattern_id
    generate_trips_for_route_and_direction(trip_table_name, table_name, journey_pattern_id)
    data = get_route_and_direction_data(trip_table_name, table_name, journey_pattern_id)
    data = convert_list_to_dataframe(data)
    generate_boxplot_for_data(data, journey_pattern_id)

    image_name = "With Outlier"
    plot_lat_long(data, journey_pattern_id, image_name)

    return data


def generate_boxplot_for_data(data, journey_pattern_id):
    print("Generating box plots")
    long = data.boxplot(column=8, whis=1.25)
    long.set_ylabel('Longitude')
    long.set_title('%s Boxplot - Longitude' % journey_pattern_id)
    file_path_long = "%s/%s Longitude Boxplot.png" % (DATA_FILE_PATH, journey_pattern_id)
    pyplot.savefig(file_path_long)
    pyplot.close()

    lat = data.boxplot(column=9, whis=1.25)
    lat.set_ylabel('Latitude')
    lat.set_title('%s Boxplot - Latitude' % journey_pattern_id)
    file_path_lat = "%s/%s Latitude Boxplot.png" % (DATA_FILE_PATH, journey_pattern_id)
    pyplot.savefig(file_path_lat)
    pyplot.close()


def get_data_without_outliers(data, journey_pattern_id):
    print("Generating data without outliers")
    q1_long = get_q1_from_boxplot(data[8])
    q3_long = get_q3_from_boxplot(data[8])
    iqr_long = get_iqr_from_boxplot(q1_long, q3_long)
    upper_whisker_long = get_upper_whisker_from_boxplot(q3_long, iqr_long)
    lower_whisker_long = get_lower_whisker_from_boxplot(q1_long, iqr_long)
    max_long = get_max_value_below_upper_whisker(data, upper_whisker_long, 8)
    min_long = get_min_value_below_upper_whisker(data, lower_whisker_long, 8)

    q1_lat = get_q1_from_boxplot(data[9])
    q3_lat = get_q3_from_boxplot(data[9])
    iqr_lat = get_iqr_from_boxplot(q1_lat, q3_lat)
    upper_whisker_lat = get_upper_whisker_from_boxplot(q3_lat, iqr_lat)
    lower_whisker_lat = get_lower_whisker_from_boxplot(q1_lat, iqr_lat)
    max_lat = get_max_value_below_upper_whisker(data, upper_whisker_lat, 9)
    min_lat = get_min_value_below_upper_whisker(data, lower_whisker_lat, 9)

    route_data = get_data_within_boundry(data, max_long, min_long, max_lat, min_lat)

    image_name = "No Outlier"
    plot_lat_long(route_data, journey_pattern_id, image_name)

    return route_data


def assign_osm_geom(data):
    latitude = data[9]
    longitude = data[8]

    osm_geom = get_osm_geom_for_point(latitude, longitude)

    osm_geom_numpy = convert_list_to_numpy(osm_geom)

    data['geom'] = pd.Series(osm_geom_numpy[:, 0, 0], index=data.index)

    return data


def get_extreme_lat_long(data):
    min_long = get_minimum(data[8])
    max_long = get_maximum(data[8])
    min_lat = get_minimum(data[9])
    max_lat = get_maximum(data[9])

    print("Max Long - %s, Min Long - %s, Max Lat - %s, Min Lat - %s" % (max_long, min_long, max_lat, min_lat))

    return max_long, min_long, max_lat, min_lat


def generate_osm_data(journey_pattern_id, min_lat, max_lat, min_long, max_long):
    print("Generating table for OSM points and line of route")
    osm_point_table_name = "osm_point_%s" % journey_pattern_id
    osm_line_table_name = "osm_line_%s" % journey_pattern_id
    create_table_of_osm_point_for_route(osm_point_table_name, min_long, max_long, min_lat, max_lat)
    create_table_of_osm_line_for_route(osm_line_table_name, min_long, max_long, min_lat, max_lat)

    return osm_point_table_name, osm_line_table_name


def generate_table_for_route(journey_pattern_id, file_path):
    print("Generating table for route data")
    table_name = "data_%s" % journey_pattern_id
    create_table_for_route_data(table_name, file_path)


def assign_nearest_osm_line_to_route_data(journey_pattern_id, osm_table_name, limit):
    print("Assigning OSM line to route data")
    data_table_name = "data_%s" % journey_pattern_id
    file_name = "data_%s_osm_line.csv" % journey_pattern_id
    start_time = get_datetime()
    print("Limit - %s" % limit)
    print("Start Time - %s" % start_time)

    route_data = get_nearest_osm_node_from_point(data_table_name, osm_table_name, limit)

    end_time = get_datetime()
    print("End Time - %s" % end_time)
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)

    create_data_file(route_data, file_name)

    return file_name


def assign_nearest_osm_point_to_route_data(journey_pattern_id, osm_table_name, limit):
    print("Assigning OSM point to route data")
    data_table_name = "data_%s" % journey_pattern_id
    file_name = "data_%s_osm_point.csv" % journey_pattern_id
    start_time = get_datetime()
    print("Limit - %s" % limit)
    print("Start Time - %s" % start_time)

    route_data = get_nearest_osm_node_from_point(data_table_name, osm_table_name, limit)

    end_time = get_datetime()
    print("End Time - %s" % end_time)
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)

    create_data_file(route_data, file_name)

    return file_name


def create_file_for_lat_long_data_for_trip(table_name, journey_pattern_id, vehicle_journey_id, file_name):
    latitude_longitude = get_lat_long_for_journey_pattern_id_and_vehicle_journey_id(table_name, journey_pattern_id,
                                                                                    vehicle_journey_id)

    create_data_file(latitude_longitude, file_name)


def get_gps_data_of_undiverted_trips(data_file_name, journey_pattern_id):
    data = get_dataframe_from_file(data_file_name)
    # Unique OSM ids grouped by trips
    unique_osm_ids_per_trip = get_dataframe_groupby(data, [4, 5, 16], 18)
    unique_trips = get_dataframe_groupby(data, [4, 5], 18)
    # List containing unique OSM id's in a trip for all trips
    trip_osm_id = get_list_of_unique_osm_ids_for_trips(unique_osm_ids_per_trip, unique_trips)
    trip_osm_id_numpy = convert_list_to_numpy(trip_osm_id)
    frequency_of_osm_id = get_frequency_of_element_in_list(trip_osm_id_numpy)
    df_frequency_osm_id = convert_list_to_dataframe(frequency_of_osm_id, set_col_name=[16, 'counts'])
    percentage_trips_to_remove = 12.5
    # data = get_rid_of_some_data(data)
    undiverted_trips, diverted_trips = get_trips_data_with_and_without_diversions(df_frequency_osm_id, data,
                                                                                  unique_trips,
                                                                                  percentage_trips_to_remove)

    image_name_undiverted, image_name_combined = get_images_name(percentage_trips_to_remove)
    generate_scatter_plot(undiverted_trips, diverted_trips, journey_pattern_id, image_name_undiverted,
                          image_name_combined, percentage_trips_to_remove)

    file_name = "cleaned_data_%s.csv" % journey_pattern_id
    write_dataframe_to_csv(undiverted_trips, file_name)

    return undiverted_trips


def get_list_of_unique_osm_ids_for_trips(unique_osm_ids_per_trip, unique_trips):
    # Adding the unique OSM ids per trip into the list
    # Making different list of OSM ids for different trips
    osm_ids = []
    for i in range(len(unique_trips.index)):
        # All unique OSM ids for a trips
        unique_osm_id = unique_osm_ids_per_trip[(unique_osm_ids_per_trip[4] == unique_trips[4][i]) &
                                                (unique_osm_ids_per_trip[5] == unique_trips[5][i])][16]

        unique_osm_id = get_list_from_dataframe(unique_osm_id)
        # Add those OSM ids into a list and generate different list for each trip
        osm_ids.append(unique_osm_id)
        print("On trip number %s" % i)

    osm_ids = get_flat_list(osm_ids)

    return osm_ids


def get_trips_data_with_and_without_diversions(dataframe, data, trips, percentage):
    print("Generating Trips with and without diversion")
    number_of_trips_diverted = len(trips.index) * percentage/100
    diverted_trips_osm_id = dataframe[dataframe['counts'] < number_of_trips_diverted]
    undiverted_trips_osm_id = dataframe[dataframe['counts'] >= number_of_trips_diverted]
    diverted_trips = pd.merge(data, diverted_trips_osm_id, on=16)
    undiverted_trips = pd.merge(data, undiverted_trips_osm_id, on=16)
    return undiverted_trips, diverted_trips


if __name__ == '__main__':
    data_cleaning('gps_data', '27B', 'O')
    # assign_nearest_osm_point_to_route_data('046A0001', 'osm_point_046A0001', 999999999)
    # get_gps_data_of_undiverted_trips('data_046A0001_osm_point.csv', '046A0001')
