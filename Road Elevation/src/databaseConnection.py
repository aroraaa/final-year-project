import psycopg2
import os
from functions import *

GPS_DATA_FOLDER = "E:/final-year-project/Data/GPS Data"
DATA_FOLDER = "../Trips_46AO"
OSM_DATA_FOLDER = "E:/final-year-project/Road Elevation/Trips_46AO"
DATABASE_NAME = "dublin_bus_gps_data_2013"
DUBLIN_OSM_DATABASE = "dublin_osm"
IRELAND_OSM_DATABASE = "osm"
USERNAME = "postgres"
PASSWORD = "database"
HOST = "127.0.0.1"
PORT = "5432"


def get_connection_with_dublin_osm_database():
    connection_with_database = psycopg2.connect(database=DUBLIN_OSM_DATABASE, user=USERNAME, password=PASSWORD, host=HOST,
                                                port=PORT)
    return connection_with_database


def get_connection_with_ireland_osm_database():
    connection_with_database = psycopg2.connect(database=IRELAND_OSM_DATABASE, user=USERNAME, password=PASSWORD, host=HOST,
                                                port=PORT)
    return connection_with_database


def get_connection_with_database():
    connection_with_database = psycopg2.connect(database=DATABASE_NAME, user=USERNAME, password=PASSWORD, host=HOST,
                                                port=PORT)
    return connection_with_database


def create_table_for_gps_data():
    connection = get_connection_with_database()
    cursor = connection.cursor()

    query = "CREATE TABLE gps_data (timestamp text, line_id INT, direction INT, journey_pattern_id text, time_frame " \
            "text, vehicle_journey_id INT, operator text, congestion INT, longitude DOUBLE PRECISION, latitude " \
            "DOUBLE PRECISION, delay INT, block_id INT, vehicle_id INT, stop_id text, at_stop INT);"

    cursor.execute(query)
    connection.commit()

    connection.close()


def insert_data_into_gps_data_table():
    connection = get_connection_with_database()
    cursor = connection.cursor()

    create_table_for_gps_data()

    for file in os.listdir(GPS_DATA_FOLDER):
        print('%s file under progress' % file)

        query = "COPY gps_data FROM '%s/%s' DELIMITERS ',' CSV;" % (GPS_DATA_FOLDER, file)
        cursor.execute(query)
        connection.commit()

    connection.close()


def create_table_for_gps_data_of_each_day(table_name):
    connection = get_connection_with_database()
    cursor = connection.cursor()

    query = "CREATE TABLE %s (timestamp text, line_id INT, direction INT, journey_pattern_id text, time_frame text, " \
            "vehicle_journey_id INT, operator text, congestion INT, longitude DOUBLE PRECISION, " \
            "latitude DOUBLE PRECISION, delay INT, block_id INT, vehicle_id INT, stop_id text, at_stop INT);" \
            % table_name

    cursor.execute(query)
    connection.commit()
    print('%s created successfully' % table_name)

    connection.close()


def insert_gps_data_for_each_day_in_tables():
    connection = get_connection_with_database()
    cursor = connection.cursor()
    i = 1

    for file in os.listdir(GPS_DATA_FOLDER):
        print('%s file under progress' % file)

        if i < 10:
            table_name = 'gps_data_2013_01_0%s' % i
        else:
            table_name = 'gps_data_2013_01_%s' % i

        create_table_for_gps_data_of_each_day(table_name)
        query = "COPY %s FROM '%s/%s' DELIMITERS ',' CSV;" % (table_name, GPS_DATA_FOLDER, file)
        cursor.execute(query)
        connection.commit()
        i += 1

    connection.close()


def get_data_from_table(table_name):
    query = "SELECT * FROM %s" % table_name
    connection = get_connection_with_database()
    cursor = connection.cursor()
    cursor.execute(query)
    data = cursor.fetchall()

    connection.close()

    return data


def generate_trips_for_route_and_direction(trip_table_name, table_name, journey_pattern_id):
    connection = get_connection_with_database()
    cursor = connection.cursor()

    query = "SELECT count(*), time_frame, vehicle_journey_id FROM %s WHERE journey_pattern_id = '%s'" \
            "GROUP BY time_frame, vehicle_journey_id HAVING count(*) > 9" % (table_name, journey_pattern_id)

    cursor.execute(query)
    trips_for_route_and_direction = cursor.fetchall()

    trips_for_route_and_direction = convert_list_to_dataframe(trips_for_route_and_direction)

    mean = get_mean(trips_for_route_and_direction[0])
    standard_deviation = get_standard_deviation(trips_for_route_and_direction[0])

    lower_limit = get_lower_limit_of_1sd(mean, standard_deviation)
    upper_limit = get_upper_limit_of_1sd(mean, standard_deviation)

    print("Mean - %s, Standard Deviation - %s, Lower Limit - %s, Upper Limit %s " %
          (mean, standard_deviation, lower_limit, upper_limit))

    query = "CREATE TABLE %s AS SELECT count(*), time_frame, vehicle_journey_id FROM %s " \
            "WHERE journey_pattern_id = '%s'GROUP BY time_frame, vehicle_journey_id " \
            "HAVING count(*) BETWEEN %s and %s" \
            % (trip_table_name, table_name, journey_pattern_id, lower_limit, upper_limit)

    cursor.execute(query)
    connection.commit()

    print("Trips table created successfully")

    connection.close()


def get_route_and_direction_data(trip_table_name, table_name, journey_pattern_id):
    connection = get_connection_with_database()
    cursor = connection.cursor()

    query = "SELECT %s.* FROM %s " \
            "JOIN %s ON %s.vehicle_journey_id = %s.vehicle_journey_id AND %s.time_frame = %s.time_frame " \
            "WHERE journey_pattern_id = '%s' ORDER BY timestamp" \
            % (table_name, table_name, trip_table_name, trip_table_name, table_name,
               trip_table_name, table_name, journey_pattern_id)

    cursor.execute(query)
    data = cursor.fetchall()
    connection.close()

    return data


def get_osm_geom_for_point(lat, long):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    osm_geom = []

    for i in range(len(lat)):
        query = "SELECT ST_SetSRID(ST_MakePoint(%s, %s),4326);" % (long[i], lat[i])

        cursor.execute(query)
        osm_geom.append(cursor.fetchall())

        print("%s : %s, %s -> %s " % (i, lat[i], long[i], osm_geom[i]))

    connection.close()

    return osm_geom


def create_table_for_route_data(table_name, file_name):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    query = "CREATE TABLE %s (timestamp text, line_id INT, direction INT, journey_pattern_id text, time_frame text, " \
            "vehicle_journey_id INT, operator text, congestion INT, longitude DOUBLE PRECISION, " \
            "latitude DOUBLE PRECISION, delay INT, block_id INT, vehicle_id INT, stop_id text, at_stop INT, " \
            "geom text);" % table_name

    cursor.execute(query)
    connection.commit()

    query = "COPY %s FROM '%s/%s' DELIMITERS ',' CSV;" % (table_name, OSM_DATA_FOLDER, file_name)

    cursor.execute(query)
    connection.commit()
    print("Table - %s generated successfully" % table_name)

    connection.close()


def get_nearest_osm_node_from_point(data_table_name, osm_table_name, limit):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    query = "select point.*, b.id as osm_id " \
            "from (select * from %s order by timestamp limit %s) point " \
            "CROSS JOIN LATERAL ( SELECT r.osm_id AS id, r.name as name, r.way as way " \
            "FROM %s r ORDER BY ST_Transform(r.way, 4326) <-> point.geom LIMIT 1 ) b" \
            % (data_table_name, limit, osm_table_name)

    cursor.execute(query)
    result = cursor.fetchall()

    return result


def create_table_of_osm_point_for_route(table_name, min_long, max_long, min_lat, max_lat):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    query = "create table %s as select * from planet_osm_point p where " \
            "ST_X(ST_Transform(p.way, 4326)) between %s and %s and " \
            "ST_Y(ST_Transform(p.way, 4326)) between %s and %s" % (table_name, min_long, max_long, min_lat, max_lat)

    cursor.execute(query)
    connection.commit()
    print("Table - %s generated successfully" % table_name)
    connection.close()


def create_table_of_osm_line_for_route(table_name, min_long, max_long, min_lat, max_lat):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    query = "create table %s as select * from " \
            "(select * from planet_osm_line where highway is not null) p " \
            "where ST_X(ST_Transform(ST_StartPoint(p.way), 4326)) between %s and %s " \
            "and ST_Y(ST_Transform(ST_StartPoint(p.way), 4326)) between %s and %s " \
            "and ST_X(ST_Transform(ST_EndPoint(p.way), 4326)) between %s and %s " \
            "and ST_Y(ST_Transform(ST_EndPoint(p.way), 4326)) between %s and %s" \
            % (table_name, min_long, max_long, min_lat, max_lat, min_long, max_long, min_lat, max_lat)

    cursor.execute(query)
    connection.commit()
    print("Table - %s generated successfully" % table_name)
    connection.close()


def get_elevation(long, lat):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()
    elevation = []

    for i in range(len(long)):
        print("Getting elevation for point: %s" % i)
        query = "SELECT ST_Value(rast, ST_Transform(ST_SetSRID(ST_Point(%s, %s), 4236), 4236)) " \
                "FROM elevation_profile " \
                "WHERE ST_Intersects(rast, ST_Transform(ST_SetSRID(ST_Point(%s, %s),4236), 4236))" \
                % (long[i], lat[i], long[i], lat[i])

        cursor.execute(query)
        result = cursor.fetchone()
        elevation.append(result)

    connection.close()
    return elevation


def get_lat_long_for_journey_pattern_id_and_vehicle_journey_id(table_name, journey_pattern_id,
                                                               vehicle_journey_id):
    connection = get_connection_with_database()
    cursor = connection.cursor()

    query = "SELECT latitude, longitude FROM %s WHERE journey_pattern_id = %s AND vehicle_journey_id = %s" % \
            (table_name, journey_pattern_id, vehicle_journey_id)

    cursor.execute(query)
    latitude_longitude = cursor.fetchall()

    connection.close()

    return latitude_longitude


def get_nearest_dublin_osm_line_from_lat_long(lat, long):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    node_info = []

    for i in range(len(lat)):
        query = "select osm_id, " \
                "ST_DistanceSphere(ST_ClosestPoint(ST_Transform(p.way, 4326), point.geom), point.geom) as distance " \
                "from (select * from planet_osm_line where highway is not NULL) p, " \
                "(select ST_SetSRID(ST_MakePoint(%s, %s), 4326) as geom) point " \
                "order by ST_Transform(p.way, 4326) <-> point.geom limit 1" % (long[i], lat[i])

        cursor.execute(query)
        node_info.append(cursor.fetchall())

        print("%s : %s, %s -> %s " % (i, lat[i], long[i], node_info[i]))

    connection.close()

    return node_info


def get_nearest_ireland_osm_line_from_lat_long(lat, long):
    connection = get_connection_with_ireland_osm_database()
    cursor = connection.cursor()

    node_info = []

    for i in range(len(lat)):
        query = "select osm_id, " \
                "ST_DistanceSphere(ST_ClosestPoint(ST_Transform(p.way, 4326), point.geom), point.geom) as distance " \
                "from (select * from planet_osm_line where highway is not NULL) p, " \
                "(select ST_SetSRID(ST_MakePoint(%s, %s), 4326) as geom) point " \
                "order by ST_Transform(p.way, 4326) <-> point.geom limit 1" % (long[i], lat[i])

        cursor.execute(query)
        node_info.append(cursor.fetchall())

        print("%s : %s, %s -> %s " % (i, lat[i], long[i], node_info[i]))

    connection.close()

    return node_info


def get_nearest_route_osm_line_from_lat_long(lat, long, table_name):
    connection = get_connection_with_dublin_osm_database()
    cursor = connection.cursor()

    node_info = []

    for i in range(len(lat)):
        query = "select osm_id, " \
                "ST_DistanceSphere(ST_ClosestPoint(ST_Transform(p.way, 4326), point.geom), point.geom) as distance " \
                "from %s p, (select ST_SetSRID(ST_MakePoint(%s, %s), 4326) as geom) point " \
                "order by ST_Transform(p.way, 4326) <-> point.geom limit 1" % (table_name, long[i], lat[i])

        cursor.execute(query)
        node_info.append(cursor.fetchall())

        print("%s : %s, %s -> %s " % (i, lat[i], long[i], node_info[i]))

    connection.close()

    return node_info


if __name__ == "__main__":
    insert_data_into_gps_data_table()


