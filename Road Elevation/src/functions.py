import pandas as pd
from matplotlib import pyplot
from datetime import datetime
import numpy as np


DATA_FILE_PATH = "../Trips_46AO"
IMAGE_FOLDER = "../Trips_46AO/Route Visualization"


def get_datetime():
    return datetime.now()


def get_minutes(timedelta):
    return timedelta.seconds/60


def convert_list_to_dataframe(data_list, set_col_name=None):
    return pd.DataFrame(data_list, columns=set_col_name)


def get_mean(df):
    return df.mean()


def get_standard_deviation(df):
    return df.std()


def get_lower_limit_of_1sd(mean, std):
    return mean - std


def get_upper_limit_of_1sd(mean, std):
    return mean + std


def get_q1_from_boxplot(data):
    return data.quantile(0.25)


def get_q3_from_boxplot(data):
    return data.quantile(0.75)


def get_iqr_from_boxplot(q1, q3):
    return q3 - q1


def get_lower_whisker_from_boxplot(q1, iqr):
    return q1 - 1.25 * iqr


def get_upper_whisker_from_boxplot(q3, iqr):
    return q3 + 1.25 * iqr


def get_max_value_below_upper_whisker(data, upper_whisker, column):
    max = data[data[column] < upper_whisker][column].max()
    return max


def get_min_value_below_upper_whisker(data, lower_whisker, column):
    min = data[data[column] > lower_whisker][column].min()
    return min


def convert_list_to_numpy(list):
    return np.array(list)


def write_dataframe_to_csv(dataframe, file_name):
    file_path = "%s/%s" % (DATA_FILE_PATH, file_name)
    dataframe.to_csv(file_path, index=False, header=False)


def get_minimum(data):
    return data.min()


def get_maximum(data):
    return data.max()


def get_dataframe_from_file(file_name):
    file_path = "%s/%s" % (DATA_FILE_PATH, file_name)
    df = pd.read_csv(file_path, header=None)
    return df


def get_data_within_boundry(data, max_long, min_long, max_lat, min_lat):
    result = data[(data[8] <= max_long) & (data[8] >= min_long) & (data[9] <= max_lat) & (data[9] >= min_lat)]
    return result


def get_frequency_of_element_in_list(array):
    unique, counts = np.unique(array, return_counts=True)
    return np.asarray((unique, counts)).T


def create_data_file(data, file_name):
    file_path = "%s/%s" % (DATA_FILE_PATH, file_name)

    with open(file_path, "w") as file:
        for i in range(len(data)):
            line = ""
            for j in range(len(data[i])):
                line += "%s," % (data[i][j])
            line = line[:-1]
            line += "\n"
            print("%s: %s" % (i, line))
            file.write(line)

    print("End Time - %s" % get_datetime())


def get_route_data_file_name(journey_pattern_id):
    file_name = "data_%s.csv" % journey_pattern_id
    return file_name


def plot_lat_long(data, journey_pattern_id, image_name):
    latitude = data[9]
    longitude = data[8]
    pyplot.scatter(longitude, latitude, s=0.15, c=(0, 0, 0))
    pyplot.title("Route %s %s " % (journey_pattern_id, image_name))
    pyplot.xlabel("Longitude")
    pyplot.ylabel("Latitude")
    pyplot.savefig("%s/%s %s.png" % (IMAGE_FOLDER, journey_pattern_id, image_name))
    pyplot.close()


def generate_scatter_plot(undiverted_trips, diverted_trips, journey_pattern_id, image_name_undiverted,
                          image_name_combined, percentage_trips_to_remove):

    undiverted = pyplot.scatter(undiverted_trips[8], undiverted_trips[9], s=0.15, c='b')
    diverted = pyplot.scatter(diverted_trips[8], diverted_trips[9], s=0.15, c='r')
    pyplot.title("Route %s %s " % (journey_pattern_id, image_name_combined))
    pyplot.xlabel('Longitude')
    pyplot.ylabel('Latitude')
    legend_undiverted = "OSM id in more than %s%% trips" % percentage_trips_to_remove
    legend_diverted = "OSM id in less than %s%% trips" % percentage_trips_to_remove
    pyplot.legend((undiverted, diverted), (legend_undiverted, legend_diverted))
    pyplot.savefig("%s/%s %s.png" % (IMAGE_FOLDER, journey_pattern_id, image_name_combined))
    pyplot.close()

    undiverted = pyplot.scatter(undiverted_trips[8], undiverted_trips[9], s=0.15, c='b')
    pyplot.title("Route %s %s " % (journey_pattern_id, image_name_undiverted))
    pyplot.xlabel('Longitude')
    pyplot.ylabel('Latitude')
    pyplot.savefig("%s/%s %s.png" % (IMAGE_FOLDER, journey_pattern_id, image_name_undiverted))
    pyplot.close()


def get_dataframe_groupby(data, column, new_column_number):
    return pd.DataFrame({new_column_number: data.groupby(column).size()}).reset_index()


def get_list_from_dataframe(data):
    return data.tolist()


def get_images_name(percentage_trips_to_remove):
    image_name_undiverted = "Un-Diverted - %s%% Diverted Trips" % percentage_trips_to_remove
    image_name_combined = "Complete - %s%% Diverted Trips" % percentage_trips_to_remove
    return image_name_undiverted, image_name_combined


def get_flat_list(list_of_list):
    flat_list = [item for sublist in list_of_list for item in sublist]
    return flat_list


def get_rid_of_some_data(df):
    df = df[(df[8] < -6.27850) | (df[8] > -6.27600) | (df[9] > 53.41710) | (df[9] < 53.41560)]
    df = df[(df[8] < -6.22620) | (df[8] > -6.22550) | (df[9] > 53.39250) | (df[9] < 53.39120)]
    return df


def get_sorted_dataframe(df, column):
    df = df.sort_values(column)
    return df


def get_long_lat_from_data(data):
    long = data[8]
    lat = data[9]
    long = get_list_from_dataframe(long)
    lat = get_list_from_dataframe(lat)
    return long, lat


def plot_road_elevation(distance, elevation, method_name, journey_pattern_id):
    pyplot.plot(distance, elevation, linestyle="-", color='b')
    pyplot.xlabel("Distance (m)")
    pyplot.ylabel("Elevation (m)")
    pyplot.title("Elevation Profile using %s for %s" % (method_name, journey_pattern_id))
    pyplot.savefig("%s/%s elevation - %s.png" % (IMAGE_FOLDER, journey_pattern_id, method_name))
    pyplot.show()
    pyplot.close()


def plot_road_elevation_comparison(distance, elevation_dem, elevation_abi, method_name, journey_pattern_id):
    pyplot.plot(distance, elevation_dem, linestyle="-", color='b', label='ABI')
    pyplot.plot(distance, elevation_abi, linestyle="-", color='r', label='ABI Smoothed')
    pyplot.legend(loc='upper left')
    pyplot.xlabel("Distance (m)")
    pyplot.ylabel("Elevation (m)")
    pyplot.title("Elevation Profile using %s for %s" % (method_name, journey_pattern_id))
    pyplot.savefig("%s/%s elevation - %s.png" % (IMAGE_FOLDER, journey_pattern_id, method_name))
    pyplot.show()
    pyplot.close()


def get_distance_from_data(data):
    distance = data[17]
    distance = get_list_from_dataframe(distance)
    return distance


def get_list_from_list_of_tuple(list_of_tuple):
    return [i[0] for i in list_of_tuple]
