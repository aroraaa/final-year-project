from functions import *
from elevationFunction import *
from databaseConnection import *
import pandas as pd
from scipy import interpolate


def road_elevation():
    data_file = "cleaned_data_027B0001.csv"
    data = get_dataframe_from_file(data_file)
    single_trip = get_single_trip_from_trips(data)
    elevation_dem = get_elevation_using_dem(single_trip)
    # elevation_abi = get_elevation_using_advanced_bilinear_interpolation(single_trip)
    # distance = get_list_from_dataframe(single_trip[17])
    # plot_road_elevation_comparison(distance, elevation_dem, elevation_abi, 'DEM vs ABI - Smoothed', '027B0001')


def get_single_trip_from_trips(data):
    unique_trips = get_dataframe_groupby(data, [4, 5], 17)
    num_of_trips = len(unique_trips.index)
    single_trip = pd.DataFrame

    for i in range(3):
        print("Trip number: %s under progress" % i)
        trip = data[(data[4] == unique_trips.iloc[i, 0]) & (data[5] == unique_trips.iloc[i, 1])]
        trip = get_trip_with_distance(trip)
        if i == 0:
            single_trip = trip
        else:
            single_trip = single_trip.append(trip, ignore_index=True)

    single_trip = get_sorted_dataframe(single_trip, [17])

    return single_trip


def get_trip_with_distance(trip_data):
    print("Generating trip with distance")
    trip_data = get_sorted_dataframe(trip_data, [0])
    longitude, latitude = get_long_lat_from_data(trip_data)
    distance = get_distance_between_gps_points(longitude, latitude)
    trip_data[17] = pd.Series(distance, index=trip_data.index)

    return trip_data


def get_elevation_using_dem(data):
    longitude, latitude = get_long_lat_from_data(data)
    distance = get_list_from_dataframe(data[17])
    elevation = get_elevation(longitude, latitude)
    elevation = get_list_from_list_of_tuple(elevation)
    # plot_road_elevation(distance, elevation, 'DEM', '027B0001')
    final_elevation = get_smoothed_elevation(distance, elevation)
    # plot_road_elevation(distance, final_elevation, 'DEM - Smoothed', '027B0001')
    plot_road_elevation_comparison(distance, elevation, final_elevation, 'ABI before and after Smoothed', '027B0001')

    return final_elevation


def get_elevation_using_advanced_bilinear_interpolation(data):
    longitude, latitude = get_long_lat_from_data(data)
    distance = get_distance_from_data(data)
    elevation = get_advanced_bilinear_elevation(longitude, latitude)
    # plot_road_elevation(distance, elevation, 'Advanced Bilinear', '027B0001')
    final_elevation = get_smoothed_elevation(distance, elevation)
    # plot_road_elevation(distance, final_elevation, 'Advanced Bilinear - Smoothed', '027B0001')
    plot_road_elevation_comparison(distance, elevation, final_elevation, 'ABI before and after Smoothed', '027B0001')
    return final_elevation


def get_advanced_bilinear_elevation(longitude, latitude):
    elevation = []
    start_time = get_datetime()
    for i in range(len(longitude)):
        print("Getting elevation for GPS probe: %s" % i)

        lon = longitude[i]
        lat = latitude[i]
        samples = 3600.0

        # Determine the grid cell containing the point
        lon_row = int(round((lon - int(lon)) * samples / 2, 0))  # ncols=1801
        lat_row = int(round((lat - int(lat)) * samples, 0))  # nrows = 3601

        # Find central vertical and horizontal axis of the containing cell
        ver_axis = int(lon) + (lon_row) / (samples / 2)
        hor_axis = int(lat) + (lat_row) / samples

        # Check which sector of the cell the point is in
        if (lon - ver_axis) >= 0:
            lon_sector = 'E'
            ver_axis_neighbor = ver_axis + 1 / (samples / 2)
        else:
            lon_sector = 'W'
            ver_axis_neighbor = ver_axis - 1 / (samples / 2)
        if (lat - hor_axis) >= 0:
            lat_sector = 'N'
            hor_axis_neighbor = hor_axis + 1 / samples
        else:
            lat_sector = 'S'
            hor_axis_neighbor = hor_axis - 1 / samples

        # Distance from neighbor cell axis
        ver_dist_neighbor = abs(lon - ver_axis_neighbor)
        hor_dist_neighbor = abs(lat - hor_axis_neighbor)

        # Weights, inversely proportional to distance
        ver_scale_neighbor = 1 - ((samples / 2) * ver_dist_neighbor)
        hor_scale_neighbor = 1 - (samples * hor_dist_neighbor)

        longitude_list = [ver_axis, ver_axis_neighbor, ver_axis, ver_axis_neighbor]
        latitude_list = [hor_axis, hor_axis, hor_axis_neighbor, hor_axis_neighbor]

        # Query elevations of the 2x2 grid cells
        elev = get_elevation(longitude_list, latitude_list)
        elev = get_list_from_list_of_tuple(elev)
        elev_self = elev[0]
        elev_ver_axis_neighbor = elev[1]
        elev_hor_axis_neighbor = elev[2]
        elev_fourth_neighbor = elev[3]

        # Elevations at four points
        elev_pt_a = (1 - ver_scale_neighbor) * elev_ver_axis_neighbor + ver_scale_neighbor * elev_fourth_neighbor
        elev_pt_c = (1 - ver_scale_neighbor) * elev_self + ver_scale_neighbor * elev_hor_axis_neighbor
        elev_pt_b = (1 - hor_scale_neighbor) * elev_self + hor_scale_neighbor * elev_ver_axis_neighbor
        elev_pt_d = (1 - hor_scale_neighbor) * elev_hor_axis_neighbor + hor_scale_neighbor * elev_fourth_neighbor

        # Elevation of the point of interest
        elev_pt = (hor_scale_neighbor * elev_pt_a + (1 - hor_scale_neighbor) * elev_pt_c + ver_scale_neighbor
                   * elev_pt_d + (1 - ver_scale_neighbor) * elev_pt_b) / 2

        elevation.append(elev_pt)

    end_time = get_datetime()
    timedelta = end_time - start_time
    print("Time Taken by interpolation- %s" % get_minutes(timedelta))
    return elevation


def get_smoothed_elevation(distance, elevation):
    desimation_rate = 100
    distance_decimate, elevation_decimate = decimate(distance, elevation, desimation_rate)

    elevation_smooth = bsgf_9pt(elevation_decimate)
    distance_decimate = bsgf_9pt2(distance_decimate)

    distance_threshold = 10

    elevation_smooth = removeoutliers2(elevation_decimate, elevation_smooth, distance_threshold)
    elevation_smooth = linear_interp(elevation_smooth)

    elevation_smooth = bsgf_9pt(elevation_smooth)
    distance_decimate = bsgf_9pt2(distance_decimate)

    f = interpolate.interp1d(distance_decimate, elevation_smooth, kind='linear', fill_value='extrapolate')
    final_elevation = f(distance)

    return final_elevation


if __name__ == "__main__":
    road_elevation()