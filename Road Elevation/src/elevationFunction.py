import pandas as pd
from math import radians, cos, sin, asin, sqrt


RADIUS_OF_EARTH = 6371000


def decimate(x, y, n):
    x_out = []
    y_out = []
    dist = 0

    for i in range(1, len(x)):
        dist = dist + (x[i] - x[i-1])
        if dist >= n or i == (len(x)-1) or i == 1:
            if i == 1:
                x_out.append(x[i-1])
                y_out.append(y[i - 1])
            x_out.append(x[i])
            y_out.append(y[i])
            dist = 0

    return x_out, y_out


def get_distance_between_gps_points(long, lat):
    long_2 = long[0]
    lat_2 = lat[0]
    distance = []
    cum_dist = 0

    distance.append(0)
    for i in range(1, len(long)):
        long_rad, lat_rad, long_2_rad, lat_2_rad = map(radians, [long[i], lat[i], long_2, lat_2])

        diff_long = long_rad - long_2_rad
        diff_lat = lat_rad - lat_2_rad

        delta = sin(diff_lat / 2) ** 2 + cos(lat_rad) * cos(lat_2_rad) * sin(diff_long / 2) ** 2
        c = 2 * asin(sqrt(delta))
        d = c * RADIUS_OF_EARTH
        cum_dist = cum_dist + d
        distance.append(cum_dist)

        long_2 = long[i]
        lat_2 = lat[i]

    return distance


def bsgf_9pt(x):

    y = []

    for i in range(len(x)):

        if (i >= 4) and (i < (len(x) - 4)):

            b1 = (1/256.0) * x[i - 4]
            b2 = (1/256.0) * x[i - 3] * 8.0
            b3 = (1/256.0) * x[i - 2] * 28.0
            b4 = (1/256.0) * x[i - 1] * 56.0
            b5 = (1/256.0) * x[i] * 70.0
            b6 = (1 / 256.0) * x[i + 1] * 56.0
            b7 = (1 / 256.0) * x[i + 2] * 28.0
            b8 = (1 / 256.0) * x[i + 3] * 8.0
            b9 = (1 / 256.0) * x[i + 4]

            s1 = (1/231.0) * x[i - 4] * -21.0
            s2 = (1/231.0) * x[i - 3] * 14.0
            s3 = (1/231.0) * x[i - 2] * 39.0
            s4 = (1/231.0) * x[i - 1] * 54.0
            s5 = (1/231.0) * x[i] * 59.0
            s6 = (1 / 231.0) * x[i + 1] * 54.0
            s7 = (1 / 231.0) * x[i + 2] * 39.0
            s8 = (1 / 231.0) * x[i + 3] * 14.0
            s9 = (1 / 231.0) * x[i + 4] * -21.0

            y1 = (b1 + s1) / 2.0
            y2 = (b2 + s2) / 2.0
            y3 = (b3 + s3) / 2.0
            y4 = (b4 + s4) / 2.0
            y5 = (b5 + s5) / 2.0
            y6 = (b6 + s6) / 2.0
            y7 = (b7 + s7) / 2.0
            y8 = (b8 + s8) / 2.0
            y9 = (b9 + s9) / 2.0

            res = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9)
            # weighted components are dependent on a quadratic expression (this is a quadratic of 5 points)
            y.append(res)

        # to account for the initial values and end values that would be left out
        # use the long run mean for a better results of filtered result

        if i < 4:
            #y.append(x[i])
            avg = (x[i] + x[i + 1] + x[i + 2] + x[i + 3] + x[i + 4]) / 5.0
            y.append(avg)

        if i >= (len(x) - 4):
            #y.append(x[i])
            avg = (x[i] + x[i - 1] + x[i - 2] + x[i - 3] + x[i - 4]) / 5.0
            y.append(avg)

    y = pd.Series(y)
    # y = y.dropna
    # y = y.tolist()
    # y = na.omit(y)
    # returns filtered data as same size as input x data
    return y


def bsgf_9pt2(x):

    y = []

    for i in range(len(x)):

        if (i >= 4) and (i < (len(x) - 4)):

            b1 = (1/256.0) * x[i - 4]
            b2 = (1/256.0) * x[i - 3] * 8.0
            b3 = (1/256.0) * x[i - 2] * 28.0
            b4 = (1/256.0) * x[i - 1] * 56.0
            b5 = (1/256.0) * x[i] * 70.0
            b6 = (1 / 256.0) * x[i + 1] * 56.0
            b7 = (1 / 256.0) * x[i + 2] * 28.0
            b8 = (1 / 256.0) * x[i + 3] * 8.0
            b9 = (1 / 256.0) * x[i + 4]

            s1 = (1/231.0) * x[i - 4] * -21.0
            s2 = (1/231.0) * x[i - 3] * 14.0
            s3 = (1/231.0) * x[i - 2] * 39.0
            s4 = (1/231.0) * x[i - 1] * 54.0
            s5 = (1/231.0) * x[i] * 59.0
            s6 = (1 / 231.0) * x[i + 1] * 54.0
            s7 = (1 / 231.0) * x[i + 2] * 39.0
            s8 = (1 / 231.0) * x[i + 3] * 14.0
            s9 = (1 / 231.0) * x[i + 4] * -21.0

            y1 = (b1 + s1) / 2.0
            y2 = (b2 + s2) / 2.0
            y3 = (b3 + s3) / 2.0
            y4 = (b4 + s4) / 2.0
            y5 = (b5 + s5) / 2.0
            y6 = (b6 + s6) / 2.0
            y7 = (b7 + s7) / 2.0
            y8 = (b8 + s8) / 2.0
            y9 = (b9 + s9) / 2.0

            res = (y1 + y2 + y3 + y4 + y5 + y6 + y7 + y8 + y9)
            # weighted components are dependent on a quadratic expression (this is a quadratic of 5 points)
            y.append(res)

        # to account for the initial values and end values that would be left out
        # use the long run mean for a better results of filtered result

        if i < 4:
            y.append(x[i])
            #avg = (x[i] + x[i + 1] + x[i + 2] + x[i + 3] + x[i + 4]) / 5.0
            #y.append(avg)

        if i >= (len(x) - 4):
            y.append(x[i])
            #avg = (x[i] + x[i - 1] + x[i - 2] + x[i - 3] + x[i - 4]) / 5.0
            #y.append(avg)

    y = pd.Series(y)
    # y = y.dropna
    # y = y.tolist()
    # y = na.omit(y)
    # returns filtered data as same size as input x data
    return y


def removeoutliers2(x, xs, threshold):
    y = []
    for i in range(len(x)):
        if abs(x[i] - xs[i]) > threshold:
            z = 0
            #print i
        else:
            z = xs[i]

        y.append(z)

    return y


def linear_interp(x):

    # set the weights of the coefficients
    w = [0.25, 0.5, 1.0, 0.5, 0.25]

    for i in range(len(x)):

        if x[i] == 0:
            interp = (x[i - 2] * w[0]) + (x[i - 1] * w[1]) + (x[i] * w[2]) + (x[i + 1] * w[3]) + (x[i + 2] * w[4])
            x[i] = interp

    return x