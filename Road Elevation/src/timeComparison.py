import pandas as pd
from databaseConnection import *
from functions import *


DATA_FILE_PATH = "../Trips_46AO"
DATA_FILE_NAME = "data_046A0001.csv"
TABLE_NAME = "osm_line_046a0001"
DATA_TABLE_NAME = "data_046a0001"


def compare_time_taken_by_query_on_different_dataset():
    dataframe = get_dataframe_from_file(DATA_FILE_NAME)
    latitude = dataframe[9].iloc[0:100]
    longitude = dataframe[8][0:100]

    print("Dataset - %s" % TABLE_NAME)
    start_time = get_datetime()
    get_nearest_route_osm_line_from_lat_long(latitude, longitude, TABLE_NAME)
    end_time = get_datetime()
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)

    print("-----------------------------------------------------------")

    print("Dataset - Dublin")
    start_time = get_datetime()
    get_nearest_dublin_osm_line_from_lat_long(latitude, longitude)
    end_time = get_datetime()
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)

    print("-----------------------------------------------------------")

    print("Dataset - Ireland")
    start_time = get_datetime()
    get_nearest_ireland_osm_line_from_lat_long(latitude, longitude)
    end_time = get_datetime()
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)


def compare_time_taken_by_different_query_on_dataset():
    dataframe = get_dataframe_from_file(DATA_FILE_NAME)
    latitude = dataframe[9].iloc[0:5000]
    longitude = dataframe[8][0:5000]

    print("Query - Iterative")
    start_time = get_datetime()
    get_nearest_route_osm_line_from_lat_long(latitude, longitude, TABLE_NAME)
    end_time = get_datetime()
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)

    print("-----------------------------------------------------------")

    # Time taken by Cross Join Lateral Join Query is already recorded

    print("Query - Cross Lateral Join")
    start_time = get_datetime()
    get_nearest_osm_line_from_point(DATA_TABLE_NAME, TABLE_NAME, 5000)
    end_time = get_datetime()
    timedelta = end_time - start_time
    time_taken_minutes = get_minutes(timedelta)
    print("Time taken in minutes - %s" % time_taken_minutes)


if __name__ == "__main__":
    compare_time_taken_by_different_query_on_dataset()