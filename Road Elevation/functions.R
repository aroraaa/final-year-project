mapRouteLeaf <- function(gps)
{
  map <-  leaflet(gps)
  map <- addTiles(map)
  map <- map %>% addCircles(lng=gps$longitude, lat=gps$latitude, group='circles',
                            weight=1, radius=5, color='black', fillColor='black',
                            #popup=total$journey_pattern_id,
                            fillOpacity=0.5, opacity=1)
  return(map)
}
